import torch, numpy as np
from torch import nn
from typing import Tuple, List



class DenseDQN(nn.Module):
    """
    Simple MLP network
    Args:
        obs_shape: observation/state size of the environment
        n_actions: number of discrete actions available in the environment
        hidden_size: size of hidden layers
    """

    def __init__(self, obs_shape: int, n_actions: int, hidden_size: int = 128):
        super().__init__()
        self.net = nn.Sequential(
            nn.Linear(obs_shape, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, hidden_size),
            nn.ReLU(),
            nn.Linear(hidden_size, n_actions)
        )

    def forward(self, x):
        return self.net(x.float())



class CNNDQN(nn.Module):
    """
    1-D Convolutional Neural network. This will be used on timeseries data.
    Args:
        obs_size: observation/state size of the environment. (Num Features, Num Lookback Period)
        n_actions: number of discrete actions available in the environment
        hidden_size: hidden size of dense layer
    """

    def __init__(self, obs_size: Tuple, n_actions: int, hparams: dict):
        super().__init__()
        cnn_layers = []

        cnn_layers.append(self.cnn_unit(obs_size[0], hparams["cnn_channels"], hparams["dropouts"]["cnn"]))
        for i in range(1, hparams["cnn_layers"]):
            cnn_layers.append(self.cnn_unit(hparams["cnn_channels"], hparams["cnn_channels"], hparams["dropouts"]["cnn"]))

        cnn_layers = nn.Sequential(*cnn_layers)
        cnn_output_size = cnn_flat_size(cnn_layers, obs_size)
        dense_layers= nn.Sequential(
            nn.Linear(cnn_output_size, hparams["hidden_size"]),
            nn.Dropout(p=hparams["dropouts"]["dense"]),
            nn.ReLU(),
            nn.Linear(hparams["hidden_size"],n_actions)
        )

        self.net = nn.Sequential(
            cnn_layers,
            nn.Flatten(),
            dense_layers
        )


    def forward(self, x):
        return self.net(x.float())


    def cnn_unit(self, in_ch: int, out_ch: int, dropout: float):
        return nn.Sequential(
            nn.Conv1d(in_ch, out_ch, 5),
            #nn.MaxPool1d(2,2),
            nn.Dropout(p=dropout),
            nn.ReLU()
        )


"""
Calculate the flattened size of the out of cnn_layers .
"""
def cnn_flat_size(cnn_net, input_shape):
    output = cnn_net(torch.ones(1,  *input_shape))
    return int(np.prod(output.size()[1:]))
