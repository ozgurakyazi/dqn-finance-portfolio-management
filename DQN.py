import numpy as np, math

from typing import Tuple, List
import torch, collections, gym
from torch import nn
from torch.optim.lr_scheduler import ReduceLROnPlateau
import torch.nn.functional as F
from torch.optim import Optimizer
from torch.utils.data import Dataset, DataLoader, IterableDataset
import pytorch_lightning as pl
from pytorch_lightning.core.decorators import auto_move_data

from rl_base_models import DenseDQN, CNNDQN

from collections import OrderedDict, deque

"""
hparams = {
    "gamma":0.99,
    "lr":0.001,
    "epsilon":1.0,
    "epsilon_min":0.01,
    "epsilon_decay":0.995,
    "eps_end":0.01,
    "eps_start":1.0,
    "eps_last_frame":1000,
    "replay_size":1000,
    "warm_start_steps":1000,
    "warm_start_size":1000,
    "episode_length":200,
    "sync_rate":10,

}
"""

class DQNLightning(pl.LightningModule):
    """ Basic DQN Model """

    def __init__(self, symbol: str, features: List, hparams: dict, env: gym.Env) -> None:
        super().__init__()
        self.hparams = hparams

        self.env = env(symbol, features=features, n_lookback = hparams["n_lookback"], horizon=hparams["horizon"])
        obs_shape = self.env.observation_space.shape
        n_actions = self.env.action_space.n

        if hparams["model"] is "Dense":
            self.net = DenseDQN(obs_shape[0], n_actions)
            self.target_net = DenseDQN(obs_shape[0], n_actions)
        elif hparams["model"] is "CNN":
            self.net = CNNDQN(obs_shape, n_actions, hparams["CNNDQN"])
            self.target_net = CNNDQN(obs_shape, n_actions, hparams["CNNDQN"])
        else:
            raise

        self.buffer = ReplayBuffer(self.hparams.replay_size)
        self.agent = Agent(self.env, self.buffer)

        self.total_reward = 0
        self.episode_reward = 0

        self.populate(self.hparams.warm_start_steps)

    def populate(self, steps: int = 1000) -> None:
        """
        Carries out several random steps through the environment to initially fill
        up the replay buffer with experiences
        Args:
            steps: number of random steps to populate the buffer with
        """
        for i in range(steps):
            self.agent.play_step(self.net, epsilon=1.0)

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Passes in a state x through the network and gets the q_values of each action as an output
        Args:
            x: environment state
        Returns:
            q values
        """
        output = self.net(x)
        return output

    def dqn_mse_loss(self, batch: Tuple[torch.Tensor, torch.Tensor]) -> torch.Tensor:
        """
        Calculates the mse loss using a mini batch from the replay buffer
        Args:
            batch: current mini batch of replay data
        Returns:
            loss
        """
        states, actions, rewards, dones, next_states = batch

        state_action_values = self.net(states).gather(1, actions.unsqueeze(-1)).squeeze(-1)

        with torch.no_grad():
            next_state_values = self.target_net(next_states).max(1)[0]
            next_state_values[dones] = 0.0
            next_state_values = next_state_values.detach()

        expected_state_action_values = next_state_values * self.hparams.gamma + rewards

        return nn.MSELoss()(state_action_values, expected_state_action_values)

    def training_step(self, batch: Tuple[torch.Tensor, torch.Tensor], nb_batch) -> OrderedDict:
        """
        Carries out a single step through the environment to update the replay buffer.
        Then calculates loss based on the minibatch recieved
        Args:
            batch: current mini batch of replay data
            nb_batch: batch number
        Returns:
            Training loss and log metrics
        """
        device = self.get_device(batch)
        epsilon = max(self.hparams.eps_end, self.hparams.eps_start - self.global_step / self.hparams.eps_last_frame)

        # step through environment with agent
        reward, done = self.agent.play_step(self.net, epsilon, device)
        self.episode_reward += reward

        # calculates training loss
        loss = self.dqn_mse_loss(batch)

        if self.trainer.use_dp or self.trainer.use_ddp2:
            loss = loss.unsqueeze(0)

        if done:
            self.total_reward = self.episode_reward
            self.episode_reward = 0

        # Soft update of target network
        if self.global_step % self.hparams.sync_rate == 0:
            self.target_net.load_state_dict(self.net.state_dict())

        self.log_dict({'total_reward': torch.tensor(self.total_reward).to(device),
               'accuracy':torch.tensor(self.env.accuracy).to(device),
               'moving_acc':torch.tensor(self.env.moving_acc).to(device),
               'epsilon':torch.tensor(epsilon).to(device),
               'loss':loss,
               }, on_step=True, on_epoch=False)

        return {'loss': loss}

    '''
    The validation is done with
    '''
    def validation_step(self, *args, **kwargs):
        self.agent.reset(test=True)

        rewards = []
        eps_len=0
        while True:
            reward, done = self.agent.play_step(self.net, 0.0, self.device)
            if done:
                break
            rewards.append(reward)
            eps_len += 1

        self.agent.reset(test=False)

        self.log("val_mean_reward", np.mean(rewards))
        self.log("val_ep_len", len(rewards))


    def configure_optimizers(self) -> List[Optimizer]:
        """ Initialize Adam optimizer"""
        optimizer = torch.optim.Adam(self.net.parameters(), lr=self.hparams.lr)
        return [optimizer]

    def train_dataloader(self) -> DataLoader:
        """Initialize the Replay Buffer dataset used for retrieving experiences"""
        dataset = RLDataset(self.buffer, self.hparams.episode_length)
        dataloader = DataLoader(dataset=dataset,
                                batch_size=self.hparams.batch_size,
                                num_workers=6
                                )
        return dataloader

    """
    Mock dataloader. Without this, pl does not run validation.
    """
    def val_dataloader(self):
        dataset = RLDataset(self.buffer, 1)
        dataloader = DataLoader(dataset=dataset,
                                batch_size=1,
                                )
        return dataloader

    def get_device(self, batch) -> str:
        """Retrieve device currently being used by minibatch"""
        return batch[0].device.index if self.on_gpu else 'cpu'


# Named tuple for storing experience steps gathered in training
Experience = collections.namedtuple(
    'Experience', field_names=['state', 'action', 'reward',
                               'done', 'new_state'])

class ReplayBuffer:
    """
    Replay Buffer for storing past experiences allowing the agent to learn from them
    Args:
        capacity: size of the buffer
    """

    def __init__(self, capacity: int) -> None:
            self.buffer = collections.deque(maxlen=capacity)

    def __len__(self) -> None:
        return len(self.buffer)

    def append(self, experience: Experience) -> None:
        """
        Add experience to the buffer
        Args:
            experience: tuple (state, action, reward, done, new_state)
        """
        self.buffer.append(experience)

    def sample(self, batch_size: int) -> Tuple:
        indices = np.random.choice(len(self.buffer), batch_size, replace=False)
        states, actions, rewards, dones, next_states = zip(*[self.buffer[idx] for idx in indices])

        return (np.array(states), np.array(actions), np.array(rewards, dtype=np.float32),
                np.array(dones, dtype=np.bool), np.array(next_states))


class RLDataset(IterableDataset):
    """
    Iterable Dataset containing the ReplayBuffer
    which will be updated with new experiences during training
    Args:
        buffer: replay buffer
        sample_size: number of experiences to sample at a time
    """

    def __init__(self, buffer: ReplayBuffer, sample_size: int = 200) -> None:
        self.buffer = buffer
        self.sample_size = sample_size

    def __iter__(self) -> Tuple:
        states, actions, rewards, dones, new_states = self.buffer.sample(self.sample_size)
        for i in range(len(dones)):
            yield states[i], actions[i], rewards[i], dones[i], new_states[i]



class Agent:
    """
    Base Agent class handeling the interaction with the environment
    Args:
        env: training environment
        replay_buffer: replay buffer storing experiences
    """

    def __init__(self, env, replay_buffer: ReplayBuffer) -> None:
        self.env = env
        self.replay_buffer = replay_buffer
        self.reset()

    def reset(self, test=False) -> None:
        """ Resents the environment and updates the state"""
        self.state = self.env.reset(test)
        self.test_time = test

    def get_action(self, net: nn.Module, epsilon: float, device: str) -> int:
        """
        Using the given network, decide what action to carry out
        using an epsilon-greedy policy
        Args:
            net: DQN network
            epsilon: value to determine likelihood of taking a random action
            device: current device
        Returns:
            action
        """
        if np.random.random() < epsilon:
            action = self.env.action_space.sample()
        else:
            state = torch.tensor([self.state])

            if device not in ['cpu']:
                state = state.cuda(device)

            q_values = net(state)
            _, action = torch.max(q_values, dim=1)
            action = int(action.item())

        return action

    @torch.no_grad()
    def play_step(self, net: nn.Module, epsilon: float = 0.0, device: str = 'cpu'):
        """
        Carries out a single interaction step between the agent and the environment
        Args:
            net: DQN network
            epsilon: value to determine likelihood of taking a random action
            device: current device
        Returns:
            reward, done
        """

        action = self.get_action(net, epsilon, device)

        # do step in the environment
        new_state, reward, done, _ = self.env.step(action)

        if not self.test_time:
            exp = Experience(self.state, action, reward, done, new_state)
            self.replay_buffer.append(exp)

        self.state = new_state
        if done:
            self.reset()
        return reward, done
