from collections import deque
import numpy as np, pandas as pd

import gym
from gym import spaces

"""
Simple finance environment where
"""
class Finance(gym.Env):
    url = 'http://hilpisch.com/aiif_eikon_eod_data.csv'
    def __init__(self, symbol, features, n_lookback, horizon):
        # Percent of training data for the timeseries. Calculated from the beginning of timeseries,
        # no shuffle
        self.train_size= 0.8

        # Number of equally distanced starting points for the training. Since we have around 10 years of data,
        # not all the points in that are experienced equally. The beginning of the timeseries is used in the learning
        # more when compared to the later parts. With this option, there will be equally distanced starting
        # points, and the problem will be avoided.
        self.n_start_point = 8

        self.symbol = symbol
        self.features = features

        obs_shape = (len(features),n_lookback) if len(features)>1 else (n_lookback,)
        self.observation_space = spaces.Box(low=-10.0, high=10.0, shape=obs_shape)
        self.action_space = spaces.Discrete(2)

        self.horizon = horizon
        self.osn = n_lookback
        self.rewards = deque(maxlen=200)  ## reward of last 200 steps, to calculate the moving average
        self.min_accuracy = 0.475 ## when the moving accuracy is below this level, the episode is terminated
        self._get_data()
        self._prepare_data()
        self.test_bar = int(self.data.shape[0]*self.train_size) ## The index where the testing should start

    def _get_data(self):
        self.raw = pd.read_csv(self.url, index_col=0,
                               parse_dates=True).dropna()
    def _prepare_data(self):
        self.data = np.log(self.raw/ self.raw.shift(1))
        self.data = (self.data - self.data.mean()) / self.data.std()
        self.data["r"] = np.log(self.raw[self.symbol]/ self.raw[self.symbol].shift(self.horizon))
        self.data.dropna(inplace=True)
        self.data['d'] = np.where(self.data["r"] > 0, 1, 0)

    def _get_state(self):
        return self.data[self.features].iloc[
            self.bar - self.osn:self.bar].values.T

    def seed(self, seed=None):
        pass

    def reset(self, test=False):
        self.treward = 0
        self.rewards.clear()
        self.accuracy = 0

        ## Calculate the index where the episode should finish. The training should never arrive to the
        ## testing data.
        ## Here also the horizon should be considered. If the horizon is for example, 1 day,
        ## then the end of data should be upper limit. If horizon is 2 days, 1 day before the end
        ## should be the limit. The general formula for this is -horizon+1
        if test:
            self.upper_limit = self.data.shape[0]-self.horizon+1 ## If test time, end of data is the limit
        else:
            self.upper_limit = self.test_bar-self.horizon+1 ## If train time, index of the test start is the limit

        self.reset_bar(test)
        
        return self._get_state()

    """
    If train time: randomly assign the start bar to one of the points, which divides the whole
        training data to the self.n_start_point  points. A simple illustration is as follows:

        P-----P-----P-----P-----#####Test####

        where example self.n_start_point = 4,
              '-': Train data
              '#': Test data
              'P': Train start points.

    If test time( test=True ), the bar is start of the test index.
    """
    def reset_bar(self, test):
        if test:
            self.bar = self.test_bar
        else:
            random_start = float(np.random.randint(0,self.n_start_point))
            start_bar = int(self.test_bar * (random_start / self.n_start_point))
            self.bar = max(self.osn, start_bar)

        self.start_point = self.bar ## This will be used for DONE condition below

    def step(self, action):
        ## By default, .iloc[self.bar] means it is horizon 1, predict tomorrow.
        ## To make the horizon self.horizon, we add self.horizon - 1.
        correct = action == self.data['d'].iloc[self.bar+self.horizon-1]
        reward = 1 if correct else 0
        self.treward += reward
        self.rewards.append(reward)
        self.bar += 1
        self.accuracy = self.treward / (self.bar - self.start_point)
        self.moving_acc = np.mean(self.rewards)
        if self.bar >= self.upper_limit:
            done = True
        elif reward == 1:
            done = False
        elif (self.moving_acc < self.min_accuracy and self.bar > self.start_point + 200):
            ## if the moving accuracy is below the limit, then finish the episode.
            done = True
        else:
            done = False
        state = self._get_state()
        info = {}
        return state, reward, done, info
